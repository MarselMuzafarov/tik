def bwt_decoder(encoded_word, position):
    # БУ декодирование
    # составляем матрицу бу - в каждой итерации добавляем закодированное слово в матрицу
    # и сортируем полученные последовательности построчно
    decode_matrix = list(sorted(encoded_word))
    for _ in range(len(encoded_word) - 1):
        for i in range(len(encoded_word)):
            decode_matrix[i] = encoded_word[i] + decode_matrix[i]
        decode_matrix.sort()
    return decode_matrix[position]


def mtf_decoder(sequence, symboltable):
    # декодер стопки книг
    # проходимся по последовательности, декодируя и добавляя только что декодированный символ в начало алфавита
    chars, pad = [], symboltable[::]
    for indx in sequence:
        char = pad[indx]
        chars.append(char)
        pad = [pad.pop(indx)] + pad
    return ''.join(chars)


def main():
    pos = int(input('Введите номер строки бу:\n'))
    mtf = [int(x) for x in input('Введите зашифрованную mtf строку:\n').split()]

    with open('alphabet', 'r') as f:
        alphabet = list(f.readline())

    bu = mtf_decoder(mtf, alphabet)
    decoded = bwt_decoder(bu, pos)

    print(f'{decoded}')


if __name__ == '__main__':
    main()
