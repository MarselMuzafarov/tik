def bwt_coder(message):
    bwt_matrix = []
    for i in range(len(message)):
        bwt_matrix.append(message[i:] + message[:i])

    bwt_matrix.sort()
    last_column = ''.join([word[-1] for word in bwt_matrix])
    return last_column, bwt_matrix.index(message)


def mtf_coder(strng, symboltable):
    sequence, pad = [], symboltable[::]
    for char in strng:
        indx = pad.index(char)
        sequence.append(indx)
        pad = [pad.pop(indx)] + pad
    return sequence


def main():
    # пример ввода с данным алфовитом beep boop beer!
    text = input('Введите кодируемую строку:\n')
    with open('alphabet', 'r') as f:
        alphabet = list(f.readline())

    bu = bwt_coder(text)
    encoded = mtf_coder(bu[0], alphabet)
    out = ''
    for i in encoded:
        out += f'{i} '

    out = out.strip()
    print(f'{out}\n{bu[0]}\n{bu[1]}')


if __name__ == '__main__':
    main()
